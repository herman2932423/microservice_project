﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Infrastructure;
using TransactionWebApi.Models;

namespace TransactionWebApi
{
    public class TransactionDbContext : DbContext
    {
        public TransactionDbContext(DbContextOptions<TransactionDbContext> dbContextOptions) : base(dbContextOptions)
        {
            try
            {
                var databaseCreator = Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;
                if (databaseCreator != null)
                {
                    if (!databaseCreator.CanConnect()) databaseCreator.Create();
                    if (!databaseCreator.HasTables()) databaseCreator.CreateTables();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /*Tables*/
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionDetail> TransactionDetails { get; set; }

        /*Relation*/
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Transaction>()
            //    .HasMany(p => p.TransactionDetails)
            //    .WithOne()
            //    .HasForeignKey(c => c.TransactionId);
            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.HasMany(d => d.TransactionDetails)
                    .WithOne()
                    .HasForeignKey(d => d.TransactionId);
            });

            //modelBuilder.Entity<TransactionDetail>(entity =>
            //{
            //    entity.HasOne(d => d.Transactions)
            //        .WithMany(p => p.TransactionDetails)
            //        .HasForeignKey(d => d.TransactionId);
            //});


        }
    }
}
