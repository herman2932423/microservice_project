using Microsoft.EntityFrameworkCore;
using TransactionWebApi;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();


/* DB Context*/
var dbHost = "LAPTOP-V2N5OE11";
var dbName = "Transaction_DB";
//var dbUserName = "sa";
var dbPassword = "mekanik12";
var connectionString = $"Data Source={dbHost};Initial Catalog={dbName};User ID=sa;Password={dbPassword};TrustServerCertificate=True";
builder.Services.AddDbContext<TransactionDbContext>(opt => opt.UseSqlServer(connectionString));
/*---*/

var app = builder.Build();


// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
