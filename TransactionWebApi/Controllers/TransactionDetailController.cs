﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TransactionWebApi.Models;

namespace TransactionWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionDetailController : ControllerBase
    {
        private readonly TransactionDbContext _transactionDbContext;

        public TransactionDetailController(TransactionDbContext transactionDbContext)
        {
            _transactionDbContext = transactionDbContext;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TransactionDetail>>> GetTransactionDetails()
        {
            return await _transactionDbContext.TransactionDetails.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TransactionDetail>> GetTransactionDetail(int id)
        {
            var transactionDetail = await _transactionDbContext.TransactionDetails.FindAsync(id);

            if (transactionDetail == null)
            {
                return NotFound();
            }

            return transactionDetail;
        }

        [HttpPost]
        public async Task<ActionResult<TransactionDetail>> PostTransactionDetail(TransactionDetail transactionDetail)
        {
            _transactionDbContext.TransactionDetails.Add(transactionDetail);
            await _transactionDbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTransactionDetail), new { id = transactionDetail.TransactionDetailId }, transactionDetail);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTransactionDetail(int id, TransactionDetail transactionDetail)
        {
            if (id != transactionDetail.TransactionDetailId)
            {
                return BadRequest();
            }

            _transactionDbContext.Entry(transactionDetail).State = EntityState.Modified;

            try
            {
                await _transactionDbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransactionDetail(int id)
        {
            var transactionDetail = await _transactionDbContext.TransactionDetails.FindAsync(id);
            if (transactionDetail == null)
            {
                return NotFound();
            }

            _transactionDbContext.TransactionDetails.Remove(transactionDetail);
            await _transactionDbContext.SaveChangesAsync();

            return NoContent();
        }

        private bool TransactionDetailExists(int id)
        {
            return _transactionDbContext.TransactionDetails.Any(e => e.TransactionDetailId == id);
        }
    }
}
