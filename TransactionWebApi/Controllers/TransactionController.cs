﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TransactionWebApi.Models;

namespace TransactionWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly TransactionDbContext _transactionDbContext;

        public TransactionController(TransactionDbContext transactionDbContext)
        {
            _transactionDbContext = transactionDbContext;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Transaction>>> GetOrders()
        {
            return await _transactionDbContext.Transactions.ToListAsync();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Transaction>> GetOrder(int id)
        {
            var order = await _transactionDbContext.Transactions.FindAsync(id);

            if (order == null)
            {
                return NotFound();
            }

            return order;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int id, Transaction transaction)
        {
            if (id != transaction.TransactionId)
            {
                return BadRequest();
            }

            _transactionDbContext.Entry(transaction).State = EntityState.Modified;

            try
            {
                await _transactionDbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionHeaderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Transaction>> PostOrder(Transaction order)
        {
            _transactionDbContext.Transactions.Add(order);
            await _transactionDbContext.SaveChangesAsync();

            return CreatedAtAction("GetOrder", new { id = order.TransactionId }, order);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransactionHeader(int id)
        {
            var transactionHeader = await _transactionDbContext.Transactions.FindAsync(id);
            if (transactionHeader == null)
            {
                return NotFound();
            }

            _transactionDbContext.Transactions.Remove(transactionHeader);
            await _transactionDbContext.SaveChangesAsync();

            return NoContent();
        }

        private bool TransactionHeaderExists(int id)
        {
            return _transactionDbContext.Transactions.Any(e => e.TransactionId == id);
        }

    }
}
