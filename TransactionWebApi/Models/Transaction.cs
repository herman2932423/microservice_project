﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TransactionWebApi.Models
{
    [Table("Transaction", Schema = "dbo")]
    public class Transaction
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Transaction_Id")]
        public int TransactionId { get; set; }
        [Column("Customer_Id")]
        public int CustomerId { get; set; }
        [Column("Transaction_Date")]
        public DateTime TransactionDate { get; set; }
        [Column("Grand_Price")]
        public decimal GrandPrice { get; set; }
        //public List<TransactionDetail> TransactionDetails { get; set; }

        public virtual ICollection<TransactionDetail> TransactionDetails { get; set; }


    }
}
