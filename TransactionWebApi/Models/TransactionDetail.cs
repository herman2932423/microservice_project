﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransactionWebApi.Models
{
    [Table("TransactionDetail", Schema = "dbo")]
    public class TransactionDetail
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("TransactionDetail_Id")]
        public int TransactionDetailId { get; set; }
        [Column("Transaction_Id")]
        public int TransactionId { get; set; }
        [Column("Product_Id")]
        public int ProductId { get; set; }
        [Column("Quantity")]
        public decimal Quantity { get; set; }
        [Column("UnitPrice")]
        public decimal UnitPrice { get; set; }
        [Column("TotalPrice")]
        public decimal TotalPrice { get; set; }


        //[ForeignKey("TransactionId")]
        //public virtual Transaction Transactions { get; set; }
    }
}
