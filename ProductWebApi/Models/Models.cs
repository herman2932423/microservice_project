﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ProductWebApi.Models
{
    [Table("Product", Schema = "dbo")]
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Product_Id")]
        public int ProductId { get; set; }

        [Column("Product_Name")]
        public string ProductName { get; set; }

        [Column("Product_Code")]
        public string ProductCode { get; set; }

        [Column("Product_Price")]
        public decimal ProductPrice { get; set; }
    }
}
