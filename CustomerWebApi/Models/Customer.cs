﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CustomerWebApi.Models
{
    [Table("Customer", Schema = "dbo")]
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Customer_Id")]
        public int CustomerId { get; set; }

        [Column("Customer_Name")]
        public string CustomerName { get; set; }

        [Column("Phone_No")]
        public string MobileNumber { get; set; }

        [Column("Email")]
        public string Email { get; set; }
    }
}
